import pytest
from unittest.mock import Mock

from app.workers_manager import MessageProcessor
from tests.conftest import AsyncMock


async def mock_successful_async_request(*args, **kwargs):
    return 200, 'OK'


@pytest.fixture()
def processor():
    worker_id = 1
    loop = Mock()
    http_client = Mock()
    http_client.get = mock_successful_async_request
    queue_client = AsyncMock()
    return MessageProcessor(worker_id, loop, http_client, queue_client)


class TestMessageProcessor:

    async def test_consume_messages(self, processor):
        await processor.consume_messages()

        assert processor.queue_client.consume_messages.called
        processor.queue_client.consume_messages.assert_called_with(
            callback=processor.process)

    async def test_wait_for_death(self, processor):
        stop_trigger = AsyncMock()
        await processor.wait_for_death(stop_trigger)

        assert stop_trigger.coro_wait.called
        assert processor.queue_client.close_connection.called

    async def test_process(self, processor):
        channel, envelope, props = AsyncMock(), Mock(), Mock()
        body = 'test_message'

        result = await processor.process(channel, body, envelope, props)

        assert channel.basic_client_ack.called
        assert result == 'msg_data: test_message, worker: 1, result: 200'
