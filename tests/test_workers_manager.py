import pytest
from unittest.mock import patch, Mock, MagicMock

from app.workers_manager import WorkersManager


@pytest.fixture()
def manager():
    loop = MagicMock()
    loop.run_in_executor = Mock()
    executor = MagicMock()
    queue_client = MagicMock()
    workers_num = 1
    return WorkersManager(loop, executor, queue_client, workers_num)


class TestWorkersManager:

    def test_init(self):
        loop = MagicMock()
        executor = MagicMock()
        queue_client = MagicMock()
        workers_num = 2

        manager = WorkersManager(
            loop, executor, queue_client, workers_num)

        assert manager.loop == loop
        assert manager.executor == executor
        assert manager.queue_client == queue_client
        assert manager.initial_workers_num == workers_num
        assert manager.workers == []
        assert manager.last_worker_id == 0

    def test_spawn_workers(self, manager):
        workers_num = 2

        with patch('asyncio.ensure_future') as ef_call:
            manager._spawn_additional_workers(workers_num)

            assert ef_call.call_count == 2
            assert manager.loop.run_in_executor.call_count == workers_num
            assert manager.last_worker_id == workers_num
            assert len(manager.workers) == workers_num

    def test_kill_workers(self, manager):
        w1, t1, w2, t2 = Mock(), Mock(), Mock(), Mock()
        w2.cancel, t2.set = Mock(), Mock()
        manager.workers = [(w1, t1), (w2, t2)]

        manager._reduce_workers_num_to(1)

        assert len(manager.workers) == 1
        assert w2.cancel.called
        assert t2.set.called
