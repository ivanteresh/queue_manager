from unittest.mock import MagicMock


class AsyncMock(MagicMock):
    """MagicMock subclass with support of async/await syntax."""

    def __await__(self):
        async def coro():
            return self.return_value
        return coro().__await__()

    async def __aiter__(self):
        return self

    async def __anext__(self):
        try:
            obj = next(self.side_effect)
        except StopIteration:
            raise StopAsyncIteration
        return obj
