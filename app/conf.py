import importlib
import os


class ImproperlyConfigured(Exception):
    """Error to signify that application configuration is invalid."""


class Settings:
    """Class to store application-wide settings."""

    def __init__(self):
        settings_module = os.environ.get('MANAGER_SETTINGS_MODULE', 'app.settings.develop')

        module = importlib.import_module(settings_module)

        self.settings_module = settings_module

        for setting_name in dir(module):
            setting_value = getattr(module, setting_name)
            if setting_name.isupper():
                setattr(self, setting_name, setting_value)


settings = Settings()
