import asyncio
import logging
from functools import wraps
from http import HTTPStatus

import aioamqp
import aiohttp
import async_timeout
from aiohttp import ClientError

from app.conf import settings


logger = logging.getLogger(__name__)


def retry_if_disconnected(retry_delay=settings.CONNECTION_RETRY_DELAY):
    def wrapper(fn):
        @wraps(fn)
        async def retry(self, *args, **kwargs):
            if not isinstance(self, AMQPClient):
                raise RuntimeError('Should decorate only AMQPClient methods.')

            try:
                return await fn(self, *args, **kwargs)
            except aioamqp.AmqpClosedConnection:
                logger.error('Connection to rabbit lost')

            await self.init_connection(retry_delay)
            return await retry(self, *args, **kwargs)
        return retry
    return wrapper


class AMQPClient:
    """AMQP queue access providing client."""

    def __init__(self, loop, queue, exchange, routing_key=''):
        self.loop = loop
        self.queue_name = queue
        self.exchange_name = exchange
        self.routing_key = routing_key
        self.transport = None
        self.protocol = None
        self.channel = None

    @classmethod
    async def connect(cls, **options):
        self = cls(**options)
        await self.init_connection()
        await self._declare()
        await self._bind_queue()
        return self

    async def init_connection(
            self, retry_delay=settings.CONNECTION_RETRY_DELAY):
        try:
            self.transport, self.protocol = await aioamqp.connect()
            self.channel = await self.protocol.channel()
        except OSError:
            logger.info(
                'Cant establish connection to rabbit. Retry in %s s.',
                retry_delay)
            await asyncio.sleep(retry_delay)
            await self.init_connection()
        except aioamqp.AmqpClosedConnection:
            raise RuntimeError('Connection aborted.')

    async def _declare(self):
        await self.channel.queue_declare(
            self.queue_name, durable=True)

        await self.channel.basic_qos(
            prefetch_count=1, prefetch_size=0, connection_global=True)

        await self.channel.exchange_declare(
            self.exchange_name, type_name='fanout', durable=True)

    async def _bind_queue(self):
        await self.channel.queue_bind(
            self.queue_name, self.exchange_name, self.routing_key)

    async def close_connection(self):
        try:
            await self.protocol.close()
        except aioamqp.AmqpClosedConnection:
            pass
        finally:
            self.transport.close()

    @retry_if_disconnected()
    async def publish_message(self, message_text):
        await self.channel.publish(
            message_text, self.exchange_name, self.routing_key)
        logger.info('Message published: %s', message_text)

    @retry_if_disconnected()
    async def consume_messages(self, callback):
        await self.channel.basic_consume(
            callback, queue_name=self.queue_name)

    @retry_if_disconnected()
    async def check_queue_status(self):
        queue = await self.channel.queue_declare(
            self.queue_name, durable=True)

        return queue


class HttpClient:
    """Simple async HTTP client."""

    def __init__(self, loop, num_retries, retry_interval, timeout):
        self.loop = loop
        self.num_retries = num_retries
        self.retry_interval = retry_interval
        self.timeout = timeout

    async def get(self, url):
        status, content = None, None

        for i in range(self.num_retries):
            # if i > 0:
            #     logger.info('Resending request. Try %s', i+1)

            with aiohttp.ClientSession() as session:
                with async_timeout.timeout(self.timeout):
                    try:
                        async with session.get(url) as response:
                            content = await response.text()
                            status = response.status
                    except ClientError:
                        pass

            if status == HTTPStatus.OK:
                break

            await asyncio.sleep(self.retry_interval)

        return status, content
