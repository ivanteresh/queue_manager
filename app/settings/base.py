# AMQP settings

EXCHANGE_NAME = 'task_exchange'
QUEUE_NAME = 'task_queue'

BASE_LOGGER = 'app'

CONNECTION_RETRY_DELAY = 2  # sec

# Publisher settings

MESSAGE_PUBLISH_DELAY = 1  # sec

# Worker settings

QUEUE_CHECK_DELAY = 4  # sec

WORKER_NUM_RETRIES = 5
WORKER_RETRY_INTERVAL = 5  # sec
WORKER_REQUEST_TIMEOUT = 5  # sec

# Http server settings

SERVER_HOST = '127.0.0.1'
SERVER_PORT = 8081


LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'base': {
            'format': '%(asctime)s %(filename)s %(message)s',
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'base',
        },
    },
    'loggers': {
        BASE_LOGGER: {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'asyncio': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'aiohttp': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
    },
    'root': {
        'level': 'ERROR',
        'handlers': ['console'],
    }
}
