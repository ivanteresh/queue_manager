import random

from aiohttp import web

from app.conf import settings
from app.logger import setup_logging


async def index(request):
    # Add good/bad response variety
    successful_response = random.choice((True, False))

    if successful_response:
        return web.Response(text='OK')
    else:
        raise web.HTTPBadRequest(reason='BAD REQUEST')


def setup_routes(app):
    app.router.add_get('/', index)


def create_application():
    app = web.Application()
    setup_routes(app)
    return app


def run():
    logger = setup_logging()
    logger.info('Starting http server.')
    app = create_application()
    web.run_app(app, host=settings.SERVER_HOST, port=settings.SERVER_PORT)


if __name__ == '__main__':
    run()
