from yarl import URL


def build_url(host, port, scheme='http'):
    url = URL.build(host=host, port=port, scheme=scheme)
    return str(url)
