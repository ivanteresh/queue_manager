import asyncio

from app.clients import AMQPClient
from app.conf import settings
from app.logger import setup_logging


class Publisher:
    def __init__(self, client):
        self.client = client

    async def start_publishing(self):
        message_text = 'test_message'

        while True:
            await asyncio.sleep(settings.MESSAGE_PUBLISH_DELAY)
            await self.client.publish_message(message_text)


def run():
    setup_logging()
    loop = asyncio.get_event_loop()

    queue_client = loop.run_until_complete(
        AMQPClient.connect(
            loop=loop, queue=settings.QUEUE_NAME,
            exchange=settings.EXCHANGE_NAME)
    )
    publisher = Publisher(client=queue_client)

    loop.run_until_complete(publisher.start_publishing())


if __name__ == '__main__':
    run()
