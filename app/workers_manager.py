import asyncio
import argparse
import logging
import os
from concurrent.futures import ThreadPoolExecutor
from http import HTTPStatus

from aioprocessing import AioEvent

from app.clients import HttpClient, AMQPClient
from app.conf import settings
from app.logger import setup_logging
from app.utils import build_url


logger = setup_logging()

REQUEST_URL = build_url(host=settings.SERVER_HOST, port=settings.SERVER_PORT)


class MessageProcessor:
    """
        Message processor class that represents worker instance.
    """

    def __init__(self, worker_id, loop, http_client, queue_client):
        self.worker_id = worker_id
        self.loop = loop
        self.http_client = http_client
        self.queue_client = queue_client

    async def consume_messages(self):
        await self.queue_client.consume_messages(callback=self.process)

    async def process(self, channel, body, envelope, properties):
        status, content = await self.http_client.get(REQUEST_URL)

        message_data = 'msg_data: {0}, worker: {1}, result: {2}'.format(
            body, self.worker_id, status)

        if status == HTTPStatus.OK:
            message_severity = logging.INFO
        else:
            message_severity = logging.ERROR

        logger.log(message_severity, message_data)

        await channel.basic_client_ack(delivery_tag=envelope.delivery_tag)
        return message_data

    async def wait_for_death(self, stop_trigger):
        await stop_trigger.coro_wait()
        await self.queue_client.close_connection()
        logger.info('Worker %s terminated.', self.worker_id)


class WorkersManager:
    """
        Workers manager. Keep logic how to run, spawn and kill workers.
    """
    def __init__(self, loop, executor, queue_client,
                 min_workers, max_workers):

        self.loop = loop
        self.executor = executor
        self.min_workers = min_workers
        self.max_workers = max_workers
        self.workers = []
        self.queue_client = queue_client
        self.last_worker_id = 0

    async def maintain_workers(self):
        while True:
            queue = await self.queue_client.check_queue_status()
            queue_load = queue['message_count']

            current_workers_num = len(self.workers)
            logger.info('Queue state: workers_num: %s, queue: %s',
                        current_workers_num, queue)

            workers_num = round(queue_load / 10)

            if workers_num < self.min_workers:
                workers_num = self.min_workers
            elif workers_num > self.max_workers:
                workers_num = self.max_workers

            if workers_num > current_workers_num:
                self._spawn_additional_workers(
                    workers_num - current_workers_num)

            elif workers_num < current_workers_num:
                self._reduce_workers_num_to(workers_num)

            await asyncio.sleep(settings.QUEUE_CHECK_DELAY)

    def _spawn_additional_workers(self, workers_num):
        logger.info('Spawn additional workers: %s', workers_num)
        for _ in range(workers_num):

            self.last_worker_id += 1
            stop_trigger = AioEvent()

            worker = asyncio.ensure_future(
                self.loop.run_in_executor(
                    self.executor,
                    self.run_worker,
                    self.last_worker_id,
                    stop_trigger
                ),
                loop=self.loop
            )
            self.workers.append((worker, stop_trigger))

    def _reduce_workers_num_to(self, workers_num):
        logger.info('Reduce workers num to: %s', workers_num)
        num_to_kill = len(self.workers) - workers_num

        for _ in range(num_to_kill):
            worker, stop_trigger = self.workers.pop()
            worker.cancel()
            stop_trigger.set()

    @staticmethod
    def run_worker(worker_id, stop_trigger):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)

        queue_client = loop.run_until_complete(AMQPClient.connect(
            loop=loop, queue=settings.QUEUE_NAME,
            exchange=settings.EXCHANGE_NAME
        ))

        http_client = HttpClient(
            loop=loop,
            num_retries=settings.WORKER_NUM_RETRIES,
            retry_interval=settings.WORKER_RETRY_INTERVAL,
            timeout=settings.WORKER_REQUEST_TIMEOUT,
        )

        message_processor = MessageProcessor(
            worker_id=worker_id,
            loop=loop,
            http_client=http_client,
            queue_client=queue_client,
        )

        loop.create_task(message_processor.consume_messages())

        try:
            loop.run_until_complete(
                message_processor.wait_for_death(stop_trigger))
        except Exception:
            logger.error('Exception in the worker`s loop.')
        finally:
            loop.close()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--min-workers', type=int, nargs='?', default=1,
                        help='Number of min consumer workers to run.')

    default_max_workers = (os.cpu_count() or 1) * 5

    parser.add_argument('-m', '--max-workers', type=int, nargs='?', default=default_max_workers,
                        help='Number of max consumer workers to run.')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    min_workers = args.min_workers
    max_workers = args.max_workers

    loop = asyncio.get_event_loop()

    executor = ThreadPoolExecutor(max_workers=max_workers)

    queue_client = loop.run_until_complete(
        AMQPClient.connect(
            loop=loop, queue=settings.QUEUE_NAME,
            exchange=settings.EXCHANGE_NAME
        )
    )
    workers_manager = WorkersManager(
        loop, executor, queue_client, min_workers, max_workers)

    loop.create_task(workers_manager.maintain_workers())

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        loop.close()
