import asyncio
import logging
import logging.config
import os


from app.conf import settings


def setup_logging(base_severity=logging.INFO):
    """Setup logging for the application."""
    logging.config.dictConfig(settings.LOGGING)
    logger = logging.getLogger(settings.BASE_LOGGER)
    logger.setLevel(base_severity)

    # asyncio debug logging if required
    if os.environ.get('PYTHONASYNCIODEBUG'):
        asyncio.get_event_loop().set_debug(True)
        logging.captureWarnings(True)

    return logger
