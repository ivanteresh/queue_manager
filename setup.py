import os
from setuptools import setup, find_packages

REPO_PATH = os.path.abspath(os.path.dirname(__file__))


def get_project_dependencies(env_type=''):
    """Get the list of packages the project depends on."""
    path_parts = ['requirements']

    if env_type:
        path_parts.append(env_type)

    deps_filepath = os.path.join(REPO_PATH, '.'.join(path_parts) + '.txt')

    try:
        with open(deps_filepath) as fp:
            return list(fp)
    except OSError:
        raise RuntimeError('Unable to inspect project dependencies')


install_requires = get_project_dependencies()


setup_args = dict(
    name='queue-manager',
    version='0.1.0',
    include_package_data=True,
    description='Queue service, supporting dynamic managing queue workers.',
    author='Ivan Tereshchuk',
    author_email='ivanteresh@gmail.com',
    url='',  # TODO
    packages=find_packages(exclude=['tests', 'tests.*']),
    install_requires=install_requires,
)
setup(**setup_args)
